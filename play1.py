#!/usr/bin/python3


import json
import pickle
from pyowm import OWM
import os.path


DEFAULT_CONFIG_FILE = 'config.json'


def read_config(filename=None):
    if not filename:
        filename = DEFAULT_CONFIG_FILE
    with open(filename, "r") as f:
        content = f.read()
        config = json.loads(content)
    return config


def fetch_example(token, filename, place):
    owm = OWM(token)
    obs = owm.weather_at_place(place)
    with open(filename, "wb") as f:
        pickle.dump(obs, f)


def use_example(filename):
    with open(filename, "rb") as f:
        obs = pickle.load(f)
    return obs


config = read_config()
API_TOKEN = config['apitoken']
EXAMPLE_FILE = config['examplefile']
PLACE = config['place']
print('OpenWeatherMap API Token is {}.'.format(API_TOKEN))

if not os.path.exists(EXAMPLE_FILE):
    print('No example data found. Gonna fetch some from the API...')
    fetch_example(API_TOKEN, EXAMPLE_FILE, PLACE)
else:
    print('Example data found on {}. I will use that instead of calling the API.'.format(EXAMPLE_FILE))
obs = use_example(EXAMPLE_FILE)
w = obs.get_weather()

print(w.get_pressure())
print(w.get_temperature(unit='celsius'))
print(w.get_detailed_status())


